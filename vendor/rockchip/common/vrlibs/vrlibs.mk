LOCAL_PATH := $(call my-dir)

ifneq (,$(filter vr stbvr, $(TARGET_BOARD_PLATFORM_PRODUCT)))
PRODUCT_COPY_FILES += \
    vendor/rockchip/common/vrlibs/armeabi-v7a/libvrtoolkit.so:system/lib/libvrtoolkit.so \
    vendor/rockchip/common/vrlibs/armeabi-v7a/libvraudio_engine.so:system/lib/libvraudio_engine.so \
    vendor/rockchip/common/vrlibs/armeabi-v7a/libvr32/libvr-jni.so:system/lib/libvr-jni.so \
    vendor/rockchip/common/vrlibs/armeabi/libvrplayer-jni.so:system/lib/libvrplayer-jni.so

endif
